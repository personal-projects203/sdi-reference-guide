# SDI Reference Guide

This site is meant to be a quick-reference guide. The goal is to review the content and remind yourself of the commands you can use in the command line (Linux) and in git, as well as other key takeaways from other class topics.